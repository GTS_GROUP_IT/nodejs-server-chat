var socketService = require('./../service/socket.service');

module.exports = function(io){

    io.on('connection', (socket) => {
        // Bắt sự kiện làm bạn đưa vào danh sách chờ kết bạn
        socket.on('make friend', (request) => {
            socketService.makeFriend(request)
            .then(response => {
                console.log(response);
            }).catch(err => {
                console.log(err);
            })
        });

        // Chấp nhận sự kiện làm bạn đưa vào danh sách bạn bè thật
        socket.on('accept friend', (request) => {
            socketService.acceptFriend(request)
            .then(response => {

            }).catch(err => {
                console.log(err);
            })
        });

        // Huỷ sự kiện làm bạn
        socket.on('cancle make friend', (request)=>{
            socketService.cancleMakeFriend(request)
            .then(response => {

            }).catch(err => {
                console.log(err);
            });
        });

        // Nhắn Tin
        socket.on('send message', (request) => {
            socketService.sendMessage(request)
            .then(response => {
                
            }).catch(err => {
                console.log(err);
            });
        });
    })
}
